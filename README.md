## Introduction

This projects is a digital dictionary of personal contacts.

### Features

- List of all Contacts grouped in categories like Friends and Colleagues.
- Navigation buttons to navigate between different groups.
- Preview of each Contact with option to update contact details.
- Backend service which returns list of all contacts

### `Start Application`

- Clone project [contacts-dictionary](https://abhijeetmane@bitbucket.org/abhijeetmane/contacts-dictionary.git)
- `npm install`
- `npm start`
- Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `Test Application`

- `npm test`
- Open [Coverage report](coverage/lcov-report/index.html) to view it in the browser.

### Technologies/Plugins Used
- React, ES6, HTML, CSS
- eslint for clean code
- react-window for virtualized list
- express, cors for REST API
  
## Solution
- We need 3 components as per design to complete our mockup development.
- I created 3 components Navigation, Contacts and Preview and used them in App component.
- I have created `activeTab` state in App component which helps to display different contact list according to selected tab.
- `Navigation` component will display Navigation tabs. Component has array called tabs which decides number of tabs to display.
- On click of tabs `activeTab` state in App component will be updated to selected tab.
- Some contacts has both groups assigned 'friend' and 'colleague' respectively. It was difficult to categorise them. I assumed no contact can be part of more than 1 group and this might be data issue. I moved such ambigius contacts to ungrouped category and did not display them either in 'friend' or 'colleague' groups.
- `Contacts` component takes in `contacts` as props which contains the active contacts list based on selected tab.
- `Contactss` component iterates through contacts prop and displayes passed contacts in scrollable list.
- I have used `react-window` plugin which works by only rendering part of a large data set(to fill the viewport).
    This helps to improve performance.
- `Contacts` component also takes `activeTab` and `updateProfile` as props which is passed to it's child component `Preview`
- `Preview` component displays contact details for selected contact from `Contacts` component.
- By default Preview is in 'read only mode'. You can change it 'edit' mode by clicking 'Edit' button in preview.
- Changes are saved as soon as you start editing form. If you change group type of contact then 'edit mode' is skipped and Preview      is back to 'read only' mode. Reason to do this is contact is moved to different group so preview is reset.
- I have created 'utilities' file which has utility functions like formatPhone, formatDate and validateForm.
- Form valiadation are in place for each input. If there is any error then changes are not reflected in `Contacts` screen.

## Out of scope
`Below things are not implemented because of time limitations`
- More responsivesness. Preview will be disabled in mobile and will be displayed as overlay on click of contact
- More test coverage for some components.
- Better form valiadations.
- If having 2 different group for contact is not data issue then option to select multiple groups in preview form as well as display contact in selected groups
- If I had option to design API structure then I would have created new key, 'groups' for each contact and value would be array of group names. It is more easy for developers to iterate and display. No need of `friend` and `colleague` as separate keys. If application has many groups then we need to add those many keys for each contact which does not sound good.
- Sorting of contacts by contact names.
- Nice icons for Edit and Done buttons instead of big texts.
- Proper spinner/loader until data is loaded from backend
- Automation tests
- IBAN number formatiing
- Option to delet contact

## Known bugs and limitations
- If more than 1 group is available for contact then considered as ungrouped
- Form validations not up to the mark
- No ellipsis for longer values