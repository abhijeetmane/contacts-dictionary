
const contacts = [
    {
        "name": "Massimo Stolker",
        "street": "Beekpunge 15",
        "city": "7761 KD Schoonebeek",
        "friend": "1",
        "phone": "0667890123",
        "dob": "1970-03-28",
        "iban": "NL13TEST0123456789"
    },
    {
        "name": "Ramazan Tienstra",
        "street": "De Zeeg 128",
        "city": "2991 EH Barendrecht",
        "colleague": "1",
        "phone": "0656789012",
        "dob": "1980-01-01",
        "iban": "NL14TEST0123456789"
    },
    {
        "name": "Serhat Bongaerts",
        "street": "Dovenetel 164",
        "city": "7322 DV Apeldoorn",
        "colleague": "1",
        "phone": "0645678901",
        "dob": "1978-12-04",
        "iban": "NL15TEST0123456789"
    },
    {
        "name": "Guilliano Kooijmans",
        "street": "Professor Krausstraat 69",
        "city": "2628 JW Delft",
        "phone": "0634567890",
        "dob": "1973-07-29",
        "iban": "NL16TEST0123456789"
    },
    {
        "name": "Gertrude Clemens",
        "street": "Meerbergsche Laan 85",
        "city": "5581 WD Waalre",
        "friend": "1",
        "phone": "0623456789",
        "dob": "1982-11-21",
        "iban": "NL17TEST0123456789"
    },
    {
        "name": "Faisel Beker",
        "street": "Voorweg 91",
        "city": "2431 AR Noorden",
        "friend": "1",
        "colleague": "1",
        "phone": "0612345678",
        "dob": "1988-04-08",
        "iban": "NL18TEST0123456789"
    }
];
const express = require('express');
const app = express();
const cors = require('cors');
const port = 3333;

app.use(cors());
app.get('/contacts', (req, res) => res.json(contacts));

app.listen(port, () => console.log(`REST APP listening on port ${port}!`));