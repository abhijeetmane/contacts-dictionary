import React from 'react';
import { render, fireEvent, waitForElement } from '@testing-library/react';
import App from './App';
import Navigation from './components/Navigation'
import Contacts from './contacts/Contacts';
import Preview from './peview/Preview';

describe('Test App component', () => {
  it('Page is loaded with Title', async () => {
    const { getByText } = render(<App />);
    const linkElement = await waitForElement(() => getByText('Contacts Dictionary'));
    expect(linkElement).toBeInTheDocument();
  });
})

describe('Test Navigation component', () => {
  it('Three tabs are loaded', async () => {
    const { getByText } = render(<Navigation />);
    const allContactsTab = await waitForElement(() => getByText('All Contacts'));
    const friendsTab = await waitForElement(() => getByText('Friends'));
    const colleagueTab = await waitForElement(() => getByText('Colleagues'));
    expect(allContactsTab).toBeInTheDocument();
    expect(friendsTab).toBeInTheDocument();
    expect(colleagueTab).toBeInTheDocument();
  });

  it('First tab is active by default', async () => {
    const { getByText } = await waitForElement(() => render(<App />));
    const allContactsTab = getByText('All Contacts');
    expect(allContactsTab.classList.contains('tab-active')).toBe(true);
  });

  it('Active tab changes to Third tab on passing props', async () => {
    const { getByText } = await waitForElement(() => render(<Navigation active={3} />));
    const colleagueTab = getByText('Colleagues');
    expect(colleagueTab.classList.contains('tab-active')).toBe(true);
  });
});

describe('Test Contacts List component', () => {
  const mockUpdateProfile = jest.fn();
  const mockContacts = [{
    "name": "Massimo Stolker",
    "street": "Beekpunge 15",
    "city": "7761 KD Schoonebeek",
    "friend": "1",
    "phone": "0667890123",
    "dob": "1970-03-28",
    "iban": "NL13TEST0123456789"
  }];
  it('Name, street and city of passed contact is displayed', async () => {
    const { getByText, getAllByText } = await waitForElement(() => render(<Contacts contacts={mockContacts} activeTab={1} updateProfile={mockUpdateProfile} />));
    expect(getAllByText('Massimo Stolker')[0]).toBeInTheDocument();
    expect(getAllByText('Beekpunge 15')[0]).toBeInTheDocument();
    expect(getAllByText('7761 KD Schoonebeek')[0]).toBeInTheDocument();
  });

  it('`active-contact` class is applied on click of any Contact in contacts list', async () => {
    const { getByTestId } = await waitForElement(() => render(<Contacts contacts={mockContacts} activeTab={1} updateProfile={mockUpdateProfile} />));
    const contactItems = getByTestId('contact-items');
    fireEvent.click(contactItems);
    const contactName = await waitForElement(() => contactItems);
    expect(contactName.classList.contains('active-contact')).toBe(true);
  });

  it('No contacts available message displayed when empty contacts are passed', async () => {
    const { getByText } = await waitForElement(() => render(<Contacts contacts={[]} activeTab={1} updateProfile={mockUpdateProfile} />));
    const noContactsNode = getByText('No Contacts added to this Group');
    expect(noContactsNode).toBeInTheDocument();
    expect(noContactsNode.classList.contains('empty-data')).toBe(true);
  });
});

describe('Test Preview component', () => {
  const mockUpdateProfile = jest.fn();
  const mockContacts = [{
    "name": "Massimo Stolker",
    "street": "Beekpunge 15",
    "city": "7761 KD Schoonebeek",
    "friend": "1",
    "phone": "0667890123",
    "dob": "1970-03-28",
    "iban": "NL13TEST0123456789"
  }];
  it('Preview details not displayed when passed contact is empty', async () => {
    const { getByText } = await waitForElement(() => render(<Preview contact={null} activeTab={1} updateProfile={mockUpdateProfile} />));
    expect(getByText('Preview not available')).toBeInTheDocument();
  });

  it('Previw details Labels are displayed when passed contact is not empty', async () => {
    const { getByText } = await waitForElement(() => render(<Preview contact={mockContacts[0]} activeTab={1} updateProfile={mockUpdateProfile} />));
    expect(getByText('Groups:')).toBeInTheDocument();
    expect(getByText('Address:')).toBeInTheDocument();
    expect(getByText('Phone:')).toBeInTheDocument();
    expect(getByText('Date of Birth:')).toBeInTheDocument();
    expect(getByText('IBAN:')).toBeInTheDocument();
  });

  it('Previw details values are displayed when passed contact is not empty', async () => {
    const { getByTestId } = await waitForElement(() => render(<Preview contact={mockContacts[0]} activeTab={1} updateProfile={mockUpdateProfile} />));
    expect(getByTestId('groups').value).toBe('friend');
    expect(getByTestId('city').value).toBe('7761 KD Schoonebeek');
    expect(getByTestId('street').value).toBe('Beekpunge 15');
    expect(getByTestId('iban').value).toBe('NL13TEST0123456789');
  });

  it('Phone and Date of Birth are displayed in proper formats when passed contact is not empty', async () => {
    const { getByTestId } = await waitForElement(() => render(<Preview contact={mockContacts[0]} activeTab={1} updateProfile={mockUpdateProfile} />));
    expect(getByTestId('phone').value).toBe('+31 (06) 67-89-01-23');
    expect(getByTestId('dob').value).toBe('March 28, 1970');
  });

  it('By Default `Edit` button is diplayed in Preview', async () => {
    const { getByText } = await waitForElement(() => render(<Preview contact={mockContacts[0]} activeTab={1} updateProfile={mockUpdateProfile} />));
    expect(getByText('Edit')).toBeInTheDocument();
  });

  it('`Done` button is displayed  on click of `Edit` button in Preview', async () => {
    const { getByText } = await waitForElement(() => render(<Preview contact={mockContacts[0]} activeTab={1} updateProfile={mockUpdateProfile} />));
    fireEvent.click(getByText('Edit'))
    expect(getByText('Done')).toBeInTheDocument();
  });

})