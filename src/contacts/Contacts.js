import React, { useState, useEffect } from 'react';
import { FixedSizeList as List } from "react-window";
import Preview from '../peview/Preview';
import './Contacts.css';

const Contacts = ({ contacts, activeTab, updateProfile }) => {

    const [selected, setSelected] = useState(0);

    useEffect(() => {
        setSelected(0);
    }, [contacts]);

    const ContactItem = ({ index, style }) => (
        <div data-testid="contact-items"
            className={`contact-items ${selected === index && 'active-contact'}`}
            style={style} onClick={() => setSelected(index)}>
            <span className='contact-item'>{contacts[index].name}</span>
            <span className='contact-item'>{contacts[index].street}</span>
            <span className='contact-item'>{contacts[index].city}</span>
        </div>
    );

    return (
        <div className='contact-dashboard'>
            <div className="contact-box">
                <List
                    className="contact-list"
                    height={420}
                    itemCount={contacts.length}
                    itemSize={50}
                    width={'100%'}>
                    {ContactItem}
                </List>
                {contacts.length === 0 && <span className='empty-data'>No Contacts added to this Group</span>}
            </div>
            <Preview contact={contacts[selected]} activeTab={activeTab} updateProfile={updateProfile} />
        </div>
    );
};

export default Contacts;