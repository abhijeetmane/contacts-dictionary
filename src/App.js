import React, { useState, useEffect } from 'react';
import NavTabs from './components/Navigation';
import Contacts from './contacts/Contacts';
import { isGrouped } from './utilities/utilities';
import './App.css';

const App = () => {

  const [contactsList, setContactsList] = useState([]);
  const [friends, setFriends] = useState([]);
  const [colleagues, setColleagues] = useState([]);
  const [activeTab, setActiveTab] = useState(1);
  const categorise = (list, type) => list.filter(contact => {
    if (!(isGrouped(contact, 'friend') && isGrouped(contact, 'colleague'))) {
      return contact[type] === '1';
    }
    return false;
  });

  const currentList = () => {
    switch (activeTab) {
      case 2:
        return friends;
      case 3:
        return colleagues;
      default:
        return contactsList;
    }
  }

  const fetchContacts = async () => {
    const res = await fetch('http://localhost:3333/contacts');
    const data = await res.json();
    setContactsList(data);
    setFriends(categorise(data, 'friend'));
    setColleagues(categorise(data, 'colleague'));
  };

  useEffect(() => {
    fetchContacts();
  }, []);

  const updateProfile = (contact, type, newValue) => {
    const newContactsList = [...contactsList];
    newContactsList.map((item) => {
      if (item.phone === contact.phone) {
        item[type] = newValue;
      }
      return item;
    });
    setContactsList(newContactsList);
    if (type === 'groups') {
      setFriends(categorise(newContactsList, 'friend'));
      setColleagues(categorise(newContactsList, 'colleague'))
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <h1>Contacts Dictionary</h1>
      </header>
      <div className="container">
        <NavTabs active={activeTab} handleTabClick={setActiveTab} />
        <Contacts contacts={currentList()} activeTab={activeTab} updateProfile={updateProfile} />
      </div>
    </div>
  );
};

export default App;
