import React, { useState, useEffect } from 'react';
import { formatPhone, formatDate, validateForm, isGrouped } from '../utilities/utilities';
import './Preview.css';

const Preview = ({ contact, activeTab, updateProfile }) => {

    const [readOnly, setReadOnly] = useState(true);
    const [error, setError] = useState({
        name: false, street: false, city: false, iban: false, phone: false, dob: false
    });

    useEffect(() => {
        setReadOnly(true);
        setError({
            name: false, street: false, city: false, iban: false, phone: false, dob: false
        })
    }, [contact, activeTab]);

    const createLabel = (htmlFor, label) => <label className='profile-groups-label' htmlFor={htmlFor}>{label}: </label>
    const createInput = (value, name, type) => <input value={value} name={name} onChange={handleChange} className={`profile-values ${(error[name] && !readOnly) && 'error'}`} readOnly={readOnly} type={type} data-testid={name} />
    const checkGroupType = () => {
        let grouptype = 'ungrouped'
        if (!(isGrouped(contact, 'friend') && isGrouped(contact, 'colleague'))) {
            grouptype = isGrouped(contact, 'friend') ? 'friend' : isGrouped(contact, 'colleague') ? 'colleague' : 'ungrouped';
        }
        return grouptype;
    };
    const handleChange = (event) => {
        let value = event.target.value;
        const type = event.target.name;
        const errorInput = validateForm(type, value);
        setError({ ...error, [type]: errorInput });

        if (type === 'groups') {
            setReadOnly(activeTab !== 1);
            delete contact['friend'];
            delete contact['colleague'];
            contact[value] = '1';
        }
        if (!errorInput) {
            updateProfile(contact, type, value);
        }
    };

    return (
        <div className={`contact-profile ${!readOnly ? 'editMode' : ''}`}>
            {contact ? <>
                <div className='profile-edit-btn' onClick={() => setReadOnly(!readOnly)}>
                    {readOnly ? 'Edit' : 'Done'}
                </div>
                <form>
                    <div className='profile-name-container'><input value={contact.name} name="name" onChange={handleChange} className={`profile-name ${(error['name'] && !readOnly) && 'error'}`}
                        readOnly={readOnly} autoFocus /></div>
                    <div className="profile-sections">
                        {createLabel('groups', 'Groups')}
                        <select value={checkGroupType()} onChange={handleChange} id="groups" name="groups" data-testid="groups"
                            className={`${readOnly ? 'profile-groups' : ''}`} disabled={readOnly}>
                            <option value="ungrouped">Not Grouped</option>
                            <option value="friend">Friends</option>
                            <option value="colleague">Colleagues</option>
                        </select>
                    </div>
                    <div className="profile-sections">
                        {createLabel('street', 'Address')}
                        <div className='profile-address'>
                            {createInput(contact.street, 'street')}
                            {createInput(contact.city, 'city')}
                        </div>
                    </div>
                    <div className="profile-sections">
                        {createLabel('phone', 'Phone')}
                        {readOnly ? createInput(formatPhone(contact.phone), 'phone') : createInput(contact.phone, 'phone', 'number')}
                    </div>
                    <div className="profile-sections">
                        {createLabel('dob', 'Date of Birth')}
                        {readOnly ? createInput(formatDate(contact.dob), 'dob') : <input type="date" name="dob" onChange={handleChange} value={contact.dob}></input>}
                    </div>
                    <div className="profile-sections">
                        {createLabel('iban', 'IBAN')}
                        {createInput(contact.iban, 'iban')}
                    </div>
                </form></> : <span className='empty-data'>Preview not available</span>}
        </div>
    )
};

export default Preview;