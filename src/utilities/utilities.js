const months = [
    'January', 'February', 'March',
    'April', 'May', 'June', 'July',
    'August', 'September', 'October',
    'November', 'December'
];

const isEmpty = val => val === '';

export const isGrouped = (contact, groupType) => contact[groupType] === '1';

export const formatPhone = (number) => {
    if (number.length >= 10) {
        number = number.slice(0, 10);
        const regExp = /(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;
        const phone = regExp.exec(number);
        return `+31 (${phone[1]}) ${phone[2]}-${phone[3]}-${phone[4]}-${phone[5]}`;
    }
    return number;
};

export const formatDate = (input) => {
    const date = new Date(input);
    return `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`;
}

export const validateForm = (type, value) => {
    let error = isEmpty(value);
    if (!error && type === 'phone') {
        error = value.length > 10;
    }
    return error;
}