import React from 'react';
import './Navigation.css';

const NavTabs = props => {

    const tabs = [
        {
            id: 1,
            name: 'All Contacts'
        }, {
            id: 2,
            name: 'Friends'
        }, {
            id: 3,
            name: 'Colleagues'
        }
    ];

    const buildNav = () => tabs.map(tab =>
        <li className={`tab ${props.active === tab.id && 'tab-active'}`}
            key={tab.name} onClick={() => props.handleTabClick(tab.id)}>
            {tab.name}</li>);

    return (
        <nav>
            <ul className='tabs'>
                {buildNav()}
            </ul>
        </nav>
    );
};

export default NavTabs;